from src.duration_helper import *
from datetime import *
from src.summary import Summary
from src.duration import Duration

def normalize_summary(raw_data: dict):
    summary = {}
    id = None
    
    if 'Meeting title' in raw_data.keys():
        duration = format_duration(raw_data['Meeting duration'])
        summary['Title'] = raw_data[ 'Meeting title']
        summary['Id'] = raw_data['Meeting title']
        summary['Attended participants'] = int(raw_data['Attended participants'])
        summary['Start Time'] = raw_data['Start time']
        summary['End Time'] = raw_data['End time']
        summary['Duration'] = duration
        
    if 'Meeting Id' in raw_data.keys():
        duration = find_duration(raw_data['Meeting Start Time'],raw_data['Meeting End Time'])
        summary['Title'] = raw_data[ 'Meeting Title']
        summary['Id'] = raw_data['Meeting Id']
        summary['Attended participants'] = int(raw_data['Total Number of Participants'])
        summary['Start Time'] = raw_data['Meeting Start Time']
        summary['End Time'] = raw_data['Meeting End Time']
        summary['Duration'] = format_duration(duration)
    return summary


def build_summary_object(raw_data: dict):
    return  Summary(
        raw_data['Title'],  
        raw_data['Id'],
        int(raw_data['Attended participants']),
        raw_data['Start Time'], 
        raw_data['End Time'],
        Duration(
            raw_data['Duration']['hours'],
            raw_data['Duration']['minutes'],
            raw_data['Duration']['seconds']))



