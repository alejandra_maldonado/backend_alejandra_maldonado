from src.participant import *
from datetime import datetime
from src.duration import *
from src.duration_helper import *

def normalize_participant(raw: dict):
    participant = {}
    if 'Name' in raw.keys():
        duration = format_duration(raw['Duration'])
        participant['Full Name'] = raw['Name']
        participant['Join time'] = raw['First Join Time']
        participant['Leave time'] = raw['Last Leave time']
        participant['In-meeting Duration'] = duration
        participant['Email'] = raw['Email']
        participant['Role'] = raw['Role']
        participant['Participant ID (UPN)'] = raw['Email']

    if 'Participant ID (UPN)' in raw.keys():
        duration = format_duration(raw['In-meeting Duration'])
        join_time = format_standar_time(raw['Join Time'])
        leave_time = format_standar_time(raw['Leave Time'])
        participant['Full Name'] = raw['Full Name']
        participant['Join time'] = join_time
        participant['Leave time'] = leave_time
        participant['In-meeting Duration'] = duration
        participant['Email'] = raw['Email']
        participant['Role'] = raw['Role']
        participant['Participant ID (UPN)'] = raw['Participant ID (UPN)']
    return participant
   
def build_participant_object(raw: dict):
    if raw.get('Full Name') == None or raw.get('Join time') == None or raw.get('Leave time') == None:
        raise ValueError('Full Name, Join time, Leave time cannot be None')
    return  Participant(
        raw['Full Name'],  
        raw['Join time'],
        raw['Leave time'],
        Duration(
            raw['In-meeting Duration']['Hours'],
            raw['In-meeting Duration']['Minutes'],
            raw['In-meeting Duration']['Seconds']),
        raw['Email'],
        raw['Role'], 
        raw['Participant ID (UPN)'])

def resolve_participant_join_last_time(raw: dict):
    join_time1 = raw[0].get('Join Time')
    join_time2 = raw[1].get('Join Time')
    leave_time1 = raw[0].get('Leave Time')
    leave_time2 = raw[1].get('Leave Time')
    duration_1 = raw[0].get('Duration')
    duration_2 = raw[1].get('Duration')
    final_join_time = comparation_join_time(join_time1,join_time2)
    final_leave_time = comparation_leave_time(leave_time1,leave_time2)
    final_duration = duration_final(duration_1,duration_2)
    participant = {
        'Name': raw[0].get('Full Name'),
        'First Join Time' : final_join_time,
        'Last Leave time':  final_leave_time,
        'In-meeting Duration': final_duration,
        'Email' : raw[0].get('Email'),
        'Role' : raw[0].get('Role'),
        'Participant ID (UPN)': raw[0].get('Participant ID (UPN)')
    }
    return participant

def comparation_join_time(join_time1,join_time2):
    date1 = datetime.strptime(join_time1,'%m/%d/%y, %I:%M:%S %p')
    date2 = datetime.strptime(join_time2,'%m/%d/%y, %I:%M:%S %p')
    if date1 < date2:
        time = str((date1.timetz()).strftime('%I:%M:%S %p'))
        date = str(date1.strftime('%m/%d/%y'))
        return f"{date}, {time.lstrip('0')}"
    time = str((date2.timetz()).strftime('%I:%M:%S %p'))
    date = str(date2.strftime('%m/%d/%y')) 
    return f"{date}, {time.lstrip('0')}"

def comparation_leave_time(leave_time1,leave_time2):
    date1 = datetime.strptime(leave_time1,'%m/%d/%y, %I:%M:%S %p')
    date2 = datetime.strptime(leave_time2,'%m/%d/%y, %I:%M:%S %p')
    if date1 > date2:
        time = str((date1.timetz()).strftime('%I:%M:%S %p'))
        date = str(date1.strftime('%m/%d/%y'))
        return f"{date}, {time.lstrip('0')}"
    time = str((date2.timetz()).strftime('%I:%M:%S %p'))
    date = str(date2.strftime('%m/%d/%y')) 
    return f"{date}, {time.lstrip('0')}"

