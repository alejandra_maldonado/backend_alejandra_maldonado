from src.duration import *

class Participant:
    
    def __init__(self, full_name,join_time,leave_time, in_meeting_duration: Duration,email,role,id):
        self.full_name = full_name
        self.join_time = join_time
        self.leave_time = leave_time
        self.in_meeting_duration = in_meeting_duration
        self.email = email
        self.role = role
        self.id = id 
        
        self.name = self.get_names()['name']
        self.middle_name = self.get_names()['middle_name']
        self.first_surname = self.get_names()['first_surname']
        self.second_surname = self.get_names()['second_surname']
    

    def get_names(self):
        
        full_name_divide = {
            'name': None,
            'middle_name': None,
            'first_surname': None,
            'second_surname': None
        }

        e_full_name = self.email.split('@')[0] 
        e_list_name = e_full_name.split('.')
        
        list_names = self.full_name.split()

        if not e_list_name[0] in list_names:
            return full_name_divide

        i_name = list_names.index(e_list_name[0]) 

        if len(list_names) == 1:
            full_name_divide['name'] = e_list_name[0]
            return full_name_divide
        i_surname = list_names.index(e_list_name[1]) 

        for i in range(len(list_names)): 
            if i == 0:
                full_name_divide['name']=list_names[i]
            if i != 0 and i >= i_name and i < i_surname:
                full_name_divide['middle_name']=list_names[i]

            if i != 0 and i > i_name and i == i_surname:
                full_name_divide['first_surname']=list_names[i]
            
            if i != 0 and i > i_name and i > i_surname and i <= len(list_names):
                full_name_divide['second_surname']=list_names[i]

        return full_name_divide