from src.attendance import Attendance
from src.summary import Summary
from src.participant import Participant
from src.duration import Duration

def build_attendance_object(raw):
    participants = []
    summary = Summary(
        raw.get('Title'),
        raw.get('Id'),
        raw.get('Attended participants'),
        raw.get('Start Time'),
        raw.get('End Time'),
        Duration(
            raw['Duration'].get('hours'),
            raw['Duration'].get('minutes'),
            raw['Duration'].get('seconds')
            )
        )
    for row in raw.get('Participants'):
        partipant = Participant(
            row.get('Full Name'),
            row.get('Join time'),
            row.get('Leave time'),
            Duration(
                row['Duration'].get('Hours'),
                row['Duration'].get('Minutes'),
                row['Duration'].get('Seconds')
                ),
            row.get('Email'),
            row.get('Role'),
            row.get('Participant ID (UPN)'))

        participants.append(partipant)
    
    return Attendance(raw.get('Start Time'), summary, participants)