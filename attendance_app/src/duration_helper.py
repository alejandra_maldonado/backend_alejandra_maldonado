
from datetime import *


def format_duration(duration_string):

    duration = {
        "hours" : 0,
        "minutes" : 0,
        "seconds" : 0
    }
    list_duration = duration_string.split(' ')
    for times in list_duration:
        if 'h' in times:
            duration['hours'] = int(times.replace('h',''))
        if 'm' in times:
            duration['minutes'] = int(times.replace('m',''))
        if 's' in times:
            duration['seconds'] = int(times.replace('s',''))
    return duration
    

def find_duration(start_time, end_time):
    date_format_str = '%m/%d/%y, %I:%M:%S %p'
    start_date = datetime.strptime(start_time, date_format_str)
    end_date =   datetime.strptime(end_time, date_format_str)
    duration = end_date - start_date
    h = duration.seconds//3600
    m = (duration.seconds//60)%60
    s = duration.seconds-(h*3600)-(m*60)
    return f"{h}h {m}m {s}s" 

def format_standar_time(string_duration):
    only_date = string_duration.split(',')[0]
    year = only_date.split('/')[2]
    new_string_duration = ''
    if len(year) == 4: 
        long_year = f'/{year}'
        short_year = f'/{year[2:4]}'
        new_string_duration = string_duration.replace(long_year,short_year)
    if len(year) == 2:
        new_string_duration = string_duration
    return new_string_duration

def duration_final (duration1,duration2):
    f_duration_1 = format_duration(duration1)
    f_duration_2 = format_duration(duration2)
    values_duration1 = [x  for x in f_duration_1.values()]
    values_duration2 = [x  for x in f_duration_2.values()]
    sum_lists=[values_duration1[i] + values_duration2[i] for i,w in enumerate(values_duration1)]
    s = sum_lists[2]
    m = sum_lists[1]
    h = sum_lists[0]
    if s >= 60:
        s = s % 60
        m = m + 1
    if m >= 60:
        m = m % 60
        h += 1
    return f'{str(h)}h {str(m)}m {str(s)}s'
