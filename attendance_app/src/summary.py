from src.duration import Duration

class Summary:

    def __init__(self, title: str, id: str, attended_participants: int, start_time: str, end_time: str, duration: Duration):
        self.title = title
        self.id = id
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration

    