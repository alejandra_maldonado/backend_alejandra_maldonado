#missing __init__ means contructor call uses default __init__()default init


class PayrollSystem:

    def calculate_payroll(self, employees):
         
        if employees is None:
            return 
        if len(employees) == 0:
            return
        
        result =[ (employee, employee.calculate_payroll()) for employee in employees]

        [print(f"Payroll for: {e}\n, Check amount: {p}") for e,p in result]

        return result
        # for employee in employees:
        #     print(f'Payroll for: {employee}')
        #     print(f'Check amount: {employee.calculate_payroll()}')