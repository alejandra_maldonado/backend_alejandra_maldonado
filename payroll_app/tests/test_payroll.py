import pytest
from payroll import PayrollSystem
from employee import Employee
from employee import AdmistrativeWorker
from employee import SalesAssociate
from employee import ManufacturingWorker

def test_calculate_payroll_none_list():

    employees = None
    payroll = PayrollSystem()

    assert payroll.calculate_payroll(employees) == None

def test_calculate_payroll_none_emptylist():

    employees = []
    payroll = PayrollSystem()

    assert payroll.calculate_payroll(employees) is None

def test_calculate_nonempty_list():

    salary_employee = AdmistrativeWorker(1, 'John Doe', 1500)
    hourly_employee = ManufacturingWorker(2, 'Jane Doe', 40, 15)
    commission_employee = SalesAssociate(3, 'Foo Fighter', 1000, 250)
    payroll = PayrollSystem()
    assert payroll.calculate_payroll([
        salary_employee,
        hourly_employee,
        commission_employee
    ]) is not None

    assert len(payroll.calculate_payroll([
        salary_employee,
        hourly_employee,
        commission_employee
    ])) == 3


