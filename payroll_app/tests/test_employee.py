import pytest
from employee import Employee
from employee import AdmistrativeWorker
from employee import SalesAssociate
from employee import ManufacturingWorker

def test_employee_expects_typeerror():
    
    match = 'Can\'t instantiate abstract class Employee with abstract method calculate_payroll'
    with pytest.raises(expected_exception=TypeError, match=match):
        Employee(100, "name")


def test_AdmistrativeWorker_constructor():
   
    administrative_worker = AdmistrativeWorker(1,'Mick',500)
    expected = 500

    assert administrative_worker is not None
    assert type(administrative_worker) is AdmistrativeWorker
    assert administrative_worker.weekly_salary == expected , "Expected weekly_salary = 500"
    assert administrative_worker.__str__() == "id: 1, name: Mick" 
    
def test_AdmistrativeWorker_calculate_payroll():

    #Positive
    administrative_worker = AdmistrativeWorker(1,'Mick',800)
    expected_salary = 800
    assert administrative_worker.calculate_payroll() == expected_salary , "Expected salary of 800"

    #Negative
    administrative_worker = AdmistrativeWorker(1,'Layza',None)
    assert administrative_worker.calculate_payroll() == None , "Expected None"
    

def test_SalesAssociate_constructor():
   
    ad_w = AdmistrativeWorker(1,'Mick',500)
    sales_associate = SalesAssociate(ad_w.id , ad_w.name , ad_w.weekly_salary , 200)

    assert sales_associate is not None
    assert type(sales_associate) is SalesAssociate
    assert sales_associate.commission == 200 , "Expected commission of 500"

def test_SalesAssociate_calculate_payroll():

    #Positive
    ad_w = AdmistrativeWorker(1,'Mick',500)
    sales_associate = SalesAssociate(ad_w.id , ad_w.name , ad_w.weekly_salary , 200)
    expected_salary = 700
    assert sales_associate.calculate_payroll() == expected_salary , "Expected salary of 700, because the commission added to the payment"

    #Negative
    ad_w = AdmistrativeWorker(1,'Layza',None)
    sales_associate = SalesAssociate(ad_w.id , ad_w.name , ad_w.weekly_salary, None)
    assert sales_associate.calculate_payroll() == None , "Expected None"

def test_ManufacturingWorker_constructor():

    manufacturing_worker = ManufacturingWorker(1, 'Layza' , 10 , 150)

    assert manufacturing_worker is not None
    assert type(manufacturing_worker) is ManufacturingWorker
    assert manufacturing_worker.hour_rate == 150 , "Expected hour rate of 15"

def test_ManufacturingWorker_calculate_payroll():
    
    #Positive
    manufacturing_worker = ManufacturingWorker(1, 'Layza' , 10 , 150)
    expected_salary = 1500
    assert manufacturing_worker.calculate_payroll() == expected_salary , "Expected salary of 1500"

    #Negative
    manufacturing_worker = ManufacturingWorker(1, 'Mick' , None , None)
    assert manufacturing_worker.calculate_payroll() == None , "Expected None"

