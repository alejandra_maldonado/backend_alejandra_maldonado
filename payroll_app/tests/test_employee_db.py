import pytest
from employee import Employee
from employee_db import EmployeeDatabase

def test_get_employees_is_not_none():
    employee_db = EmployeeDatabase()
    assert employee_db.get_employees() is not None
  
           
def test_get_employees_employee_is_not_none():
    employee_db = EmployeeDatabase().get_employees()
    assert all(employee is not None for employee in employee_db)
